#!/bin/bash
# Author: Alexander Rydekull <rydekull@redhat.com>
#  
# Essentially, sick of having to manually download CLI wherever I wanted to use it. so created this script to make it easier for me.
#

if [ $(id -u) != "0" ]
then
  echo "Need to run as root" && exit 99
fi

SRC_DIR=/usr/local/src
BIN_DIR=/usr/local/bin

if [ "$1" = "-r" ] || [ "$1" = "--release" ]
then
  OCP_CLI_VERSION="$2"
fi

function list_versions() {
  OCP_CLI_VERSIONS=$(curl -sk https://github.com/openshift/origin/releases | awk -F ">" '$0 ~ /\/releases\/tag\// { gsub("</a",""); print $2 }' | grep -v ^$)
}

function download_cli() {
  list_versions
  if [ -z "${OCP_CLI_VERSION}" ]
  then
    OCP_CLI_VERSION="$(echo $OCP_CLI_VERSIONS | sed 's/ /\n/g' | grep -v rc | head -n 1)"
  else
    if [ "$(echo ${OCP_CLI_VERSIONS} | sed 's/ /\n/g' | grep -c ^${OCP_CLI_VERSION}$)" != "1" ]
    then
      echo "Could not find that version, the following versions are available:"
      echo ${OCP_CLI_VERSIONS}
      exit 1
    fi
  fi
  OS=linux
  OCP_CLI_RELATIVE_URL=$(curl -sk https://github.com/openshift/origin/releases/tag/${OCP_CLI_VERSION} | awk -F'"' '$2 ~ /\/download\// && $2 ~ /'${OS}'/ && $2 ~ /client-tools/ { print $2 }')
  OCP_CLI_FULL_URL="https://github.com${OCP_CLI_RELATIVE_URL}"

  TAR_NAME=$(basename "${OCP_CLI_FULL_URL}")
  [ -d "${SRC_DIR}" ] || mkdir -p "${SRC_DIR}"
  [ -d "${BIN_DIR}" ] || mkdir -p "${BIN_DIR}"
  cd "${SRC_DIR}" && { curl -s -J -L "$OCP_CLI_FULL_URL" > "${SRC_DIR}/${TAR_NAME}" ; cd -; } 2>&1 > /dev/null
  if [ -f /etc/debian_version ]
  then
    TAR_ARGUMENTS=" --wildcards "
  fi
  tar ${TAR_ARGUMENTS} --warning=none -zxf "${SRC_DIR}/${TAR_NAME}" '*/oc' -O > "${BIN_DIR}/oc-${OCP_CLI_VERSION}" 
  chmod a+x "${BIN_DIR}/oc-${OCP_CLI_VERSION}"
}

function symlink_latest_oc_version
{
  OCP_LATEST_CLI_VERSION=$(ls "${BIN_DIR}"/oc* | sort --version-sort | tail -n 1)
  ln -sf "${OCP_LATEST_CLI_VERSION}" "${BIN_DIR}"/oc
}

download_cli
symlink_latest_oc_version
